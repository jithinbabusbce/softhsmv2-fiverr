## To build and run container use below command
```
docker-compose up -d
```

## For entering container
```
docker ps -a
docker exec -it SoftHSMv2-container bash
```
